'use strict';

var defaultPaths   = require('../../../../../../../dev/tools/grunt/configs/path'),
    _              = require('underscore'),
    syncName       = 'whistlefish',
    themeName      = 'whistlefish',
    url            = 'http://mag2.dev/',
    themePath      = './app/design/frontend/PushON/'+themeName+'/web';

var pushonPaths = {
  project: {
    name      : themeName,
    syncName  : syncName,
    url       : url,
    sass      : themePath+'/scss',
    bin       : themePath+'/scss/bin',
    css       : themePath+'/css',
    images    : themePath+'/images',
    js        : themePath+'/js',
    fonts     : themePath+'/fonts',
    tmp       : '.tmp'
  }
};

module.exports = _.extend(defaultPaths, pushonPaths);