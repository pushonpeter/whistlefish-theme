'use strict';

var defaultCopy = require('../../../../../../../dev/tools/grunt/configs/copy'),
    _ = require('underscore');

var pushonCopy = {
    theme: {
        files: [
            {
                src: './bower_components/slick-carousel/slick/slick.js',
                dest: '<%= path.project.js %>/vendors/slick.js'
            }
        ]
    }
};

module.exports = _.extend(defaultCopy, pushonCopy);