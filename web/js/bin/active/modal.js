require(
    [
        'jquery',
        'Magento_Ui/js/modal/modal'
    ],
    function ($, modal) {

        'use strict';

        var options = {
            type: 'popup',
            responsive: true,
            innerScroll: true,
            modalClass: 'bundle-options-modal'
        };

        var popup = modal(options, $('#product_addtocart_form'));
        $("#bundle-slide").on("click", function () {
            $('#product_addtocart_form').modal('openModal');
        });

    }
);

console.log('hello worlds')